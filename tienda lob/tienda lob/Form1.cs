﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace tienda_lob
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            conexion_tienda_lob.conectar();
            conexion_tiendas.conectar();
            conexion_modelos.conectar();
            MessageBox.Show("conexion exitosa");

            dataGridView1.DataSource = llenar_grid();
            dataGridView2.DataSource = llenar_grid1();
            dataGridView3.DataSource = llenar_grid2();
        }
        public DataTable llenar_grid()
        {
            conexion_tienda_lob.conectar();
            DataTable dt = new DataTable();
            string consulta = "SELECT * FROM COLORES";
            SqlCommand cmd = new SqlCommand(consulta, conexion_tienda_lob.conectar());


            SqlDataAdapter da = new SqlDataAdapter(cmd);

            da.Fill(dt);
            return dt;

        }
        public DataTable llenar_grid1()
        {
            DataTable df = new DataTable();
            string consulta1 = "SELECT * FROM TIENDAS1";
            SqlCommand cnd = new SqlCommand(consulta1, conexion_tiendas.conectar());

            SqlDataAdapter de = new SqlDataAdapter(cnd);

            de.Fill(df);
            return df;

        }
        public DataTable llenar_grid2()
        {
            DataTable dg = new DataTable();
            string consulta1 = "SELECT * FROM MODELOS1";
            SqlCommand cvd = new SqlCommand(consulta1, conexion_modelos.conectar());

            SqlDataAdapter db = new SqlDataAdapter(cvd);

            db.Fill(dg);
            return dg;

        }
        private void button1_Click(object sender, EventArgs e)
        {
            conexion_tienda_lob.conectar();
            string insertar = "INSERT INTO COLORES (CC_NUM_COL_N,CC_DES_COL_N)VALUES(@CC_NUM_COL_N,@CC_DES_COL_N)";
            SqlCommand cmd1 = new SqlCommand(insertar, conexion_tienda_lob.conectar());
            cmd1.Parameters.AddWithValue("@CC_NUM_COL_N", textBox1.Text);
            cmd1.Parameters.AddWithValue("@CC_DES_COL_N", textBox2.Text);

            cmd1.ExecuteNonQuery();


            MessageBox.Show("se agregaron los datos");

            dataGridView1.DataSource = llenar_grid();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                textBox1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                textBox2.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            }

            catch { }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            conexion_tienda_lob.conectar();
            string actualizar = "UPDATE COLORES SET CC_NUM_COL_N=@CC_NUM_COL_N, CC_DES_COL_N=@CC_DES_COL_N";
            SqlCommand cmd2 = new SqlCommand(actualizar, conexion_tienda_lob.conectar());

            cmd2.Parameters.AddWithValue("@ CC_NUM_COL_N", textBox1.Text);
            cmd2.Parameters.AddWithValue("@ CC_DES_COL_N", textBox2.Text);

            cmd2.ExecuteNonQuery();

            MessageBox.Show("se a modificado los datos");
            dataGridView1.DataSource = llenar_grid();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            conexion_tienda_lob.conectar();
            string eliminar = "DELETE FROM COLORES WHERE CC_NUM_COL_N=@CC_NUM_COL_N";
            SqlCommand cmd3 = new SqlCommand(eliminar, conexion_tienda_lob.conectar());

            cmd3.ExecuteNonQuery();

            MessageBox.Show("se a eliminado el dato");
            dataGridView1.DataSource = llenar_grid();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox2.Clear();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            conexion_tiendas.conectar();
            string insertar = "INSERT INTO TIENDAS1 (CT_NUM_TDA_N,CT_DES_TDA_C,CT_SIG_NOT_N,CT_SIG_DEV_N)VALUES(@CT_NUM_TDA_N,@CT_DES_TDA_C,@CT_SIG_NOT_N,@CT_SIG_DEV_N)";
            SqlCommand cnd1 = new SqlCommand(insertar, conexion_tiendas.conectar());
            cnd1.Parameters.AddWithValue("@CT_NUM_TDA_N", textBox3.Text);
            cnd1.Parameters.AddWithValue("@CT_DES_TDA_C", textBox4.Text);
            cnd1.Parameters.AddWithValue("@CT_SIG_NOT_N", textBox5.Text);
            cnd1.Parameters.AddWithValue("@CT_SIG_DEV_N", textBox6.Text);

            cnd1.ExecuteNonQuery();


            MessageBox.Show("se agregaron los datos");

            dataGridView2.DataSource = llenar_grid1();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            conexion_tiendas.conectar();
            string actualizar = "UPDATE TIENDAS1 SET CT_NUM_TDA_N=@CT_NUM_TDA_N, CT_DES_TDA_C=@CT_DES_TDA_C, CT_SIG_NOT_N=@CT_SIG_NOT_N, CT_SIG_DEV_N=@CT_SIG_DEV_N";
            SqlCommand cnd2 = new SqlCommand(actualizar, conexion_tiendas.conectar());

            cnd2.Parameters.AddWithValue("@CT_NUM_TDA_N", textBox3.Text);
            cnd2.Parameters.AddWithValue("@CT_DES_TDA_C", textBox4.Text);
            cnd2.Parameters.AddWithValue("@CT_SIG_NOT_N", textBox5.Text);
            cnd2.Parameters.AddWithValue("@CT_SIG_DEV_N", textBox6.Text);

            cnd2.ExecuteNonQuery();

            MessageBox.Show("se a modificado los datos");
            dataGridView2.DataSource = llenar_grid1();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            conexion_tiendas.conectar();
            string eliminar = "DELETE FROM TIENDAS1 WHERE CT_NUM_TDA_N_N=@CT_NUM_TDA_N";
            SqlCommand cnd3 = new SqlCommand(eliminar, conexion_tiendas.conectar());

            cnd3.ExecuteNonQuery();

            MessageBox.Show("se a eliminado el dato");
            dataGridView2.DataSource = llenar_grid1();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            textBox6.Clear();
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                textBox3.Text = dataGridView2.CurrentRow.Cells[0].Value.ToString();
                textBox4.Text = dataGridView2.CurrentRow.Cells[1].Value.ToString();
                textBox5.Text = dataGridView2.CurrentRow.Cells[2].Value.ToString();
                textBox6.Text = dataGridView2.CurrentRow.Cells[3].Value.ToString();
            }

            catch { }
        }

        private void dataGridView3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                textBox7.Text = dataGridView3.CurrentRow.Cells[0].Value.ToString();
                textBox8.Text = dataGridView3.CurrentRow.Cells[1].Value.ToString();
                textBox9.Text = dataGridView3.CurrentRow.Cells[2].Value.ToString();
                textBox10.Text = dataGridView3.CurrentRow.Cells[3].Value.ToString();
                textBox11.Text = dataGridView3.CurrentRow.Cells[4].Value.ToString();
                textBox12.Text = dataGridView3.CurrentRow.Cells[5].Value.ToString();
            }

            catch { }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            conexion_modelos.conectar();
            string insertar = "INSERT INTO MODELOS1 (CM_NUM_MOD_C,CM_DES_MOD_C,CM_DNL_CAB_C,CM_DES_TEM_C,CC_NUM_COL_N,CM_PRE_DUB_C)VALUES(@CM_NUM_MOD_C,@CM_DES_MOD_C,@CM_DNL_CAB_C,@CM_DES_TEM_C,@CC_NUM_COL_N,@CM_PRE_DUB_C)";
            SqlCommand cvd1 = new SqlCommand(insertar, conexion_tienda_lob.conectar());
            cvd1.Parameters.AddWithValue("@CM_NUM_MOD_C", textBox7.Text);
            cvd1.Parameters.AddWithValue("@CM_DES_MOD_C", textBox8.Text);
            cvd1.Parameters.AddWithValue("@CM_DNL_CAB_C", textBox9.Text);
            cvd1.Parameters.AddWithValue("@CM_DES_TEM_C", textBox10.Text);
            cvd1.Parameters.AddWithValue("@CC_NUM_COL_N", textBox11.Text);
            cvd1.Parameters.AddWithValue("@CM_PRE_DUB_C", textBox12.Text);

            cvd1.ExecuteNonQuery();


            MessageBox.Show("se agregaron los datos");

            dataGridView3.DataSource = llenar_grid2();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            conexion_modelos.conectar();
            string actualizar = "UPDATE MODELOS1 SET CM_NUM_MOD_C=@CM_NUM_MOD_C, CM_DES_MOD_C=@CM_DES_MOD_C, CM_DNL_CAB_C=@CM_DNL_CAB_C, CM_DES_TEM_C=@CM_DES_TEM_C, CC_NUM_COL_N=@CC_NUM_COL_N, CM_PRE_DUB_C=@CM_PRE_DUB_C";
            SqlCommand cvd2 = new SqlCommand(actualizar, conexion_modelos.conectar());

            cvd2.Parameters.AddWithValue("@CM_NUM_MOD_C", textBox7.Text);
            cvd2.Parameters.AddWithValue("@CM_DES_MOD_C", textBox8.Text);
            cvd2.Parameters.AddWithValue("@CM_DNL_CAB_C", textBox9.Text);
            cvd2.Parameters.AddWithValue("@CM_DES_TEM_C", textBox10.Text);
            cvd2.Parameters.AddWithValue("@CC_NUM_COL_N", textBox11.Text);
            cvd2.Parameters.AddWithValue("@CM_PRE_DUB_C", textBox12.Text);

            cvd2.ExecuteNonQuery();

            MessageBox.Show("se a modificado los datos");
            dataGridView3.DataSource = llenar_grid2();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            conexion_modelos.conectar();
            string eliminar = "DELETE FROM MODELOS1 WHERE CM_NUM_MOD_C=@CM_NUM_MOD_C";
            SqlCommand cvd3 = new SqlCommand(eliminar, conexion_modelos.conectar());

            cvd3.ExecuteNonQuery();

            MessageBox.Show("se a eliminado el dato");
            dataGridView3.DataSource = llenar_grid2();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            textBox7.Clear();
            textBox8.Clear();
            textBox9.Clear();
            textBox10.Clear();
            textBox11.Clear();
            textBox12.Clear();
        }
    }
}
